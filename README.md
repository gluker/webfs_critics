# Filer's Design/Implementation
- анализ текущей программной реализации
- предложения по доработке 


## Текущая реализация
Текущая реализация предполагает работу "поверх" пустой или существующей файловой иерархии. 

При запуске программы инициализируется "контекст": `ctx := &AppContext{}`, являющийся указателем на структуру:
```golang
type AppContext struct {
        config      map[string]string
        db          *sqlx.DB
        fileobjects FileobjectPool
        users       []*User
        logger      *Logger
}
```
В дальнейшем `*ctx` передается остальным функциям и методам, являясь единым глобальным хранилищем общих данных. 

`ctx.fileobjects` -- структура: 

```golang
type FileobjectPool struct {
        fileMap map[string]*Fileobject /* relative_filepath is the key */
}
```

в которой `fileMap` -- хеш-таблица (`map` в golang), где ключом является путь к файлу (`string`), значением -- указатель (`*Fileobject`) на соответствующую структуру, которая собственно и содержит данные о файлах, ссылках (symlinks) или директориях: 

```golang
type Fileobject struct {
        Name, Ext, Description, Directory, Filepath, RelativeDirectory, RelativeFilepath, Type    string
        Size              int64
        Inode             uint64
        AccessPolicy      *FileAccessPolicy
        Parent            *Fileobject
        RuntimeMetadata struct {
            VirtualOf *Fileobject
            Virtuals  []*Fileobject
        }
        Children          []*Fileobject
}
```

Заполнение `ctx.fileobjects` осуществляется при старте, изменения -- при всех операциях, кроме чтения и добавления данных в уже существующий файл сл. образом, пример:

```golang
// RebuildStorageIndex building a directory/file structure from a starting rootPath location
RebuildStorageIndex(directory string, files *[]*Fileobject, rootDirectory string, verbose bool, recursive bool) {
    ...
    filesInDirectory = ioutil.ReadDir(directory)                        // I/O DISK
    for _, file := range filesInDirectory {
        file.FileMode()                                                 // I/O DISK
        file.Sys().(*syscall.Stat_t)                                    // I/O DISK
    }
}

// InjectFileObjects injecting fileobjects into the pool, ctx.fileobjects == pool 
InjectFileObjects(ctx *AppContext, fileobjects []*Fileobject, resetMaps bool) {
    for _, f := range fileobjects {
        if _, ok := pool.fileMap[f.RelativeFilepath]; ok {              // READ LOCK
            ...EjectFileObjects(ctx, []*Fileobject{f}) {
                ...
                delete(ctx.fileobjects.fileMap, ff.RelativeFilepath)    // WRITE LOCK
            }
            ...fileMap[f.RelativeFilepath] = f                          // WRITE LOCK
        }
        
    for _, f := range fileobjects { 
        if parent, ok := pool.fileMap[f.RelativeDirectory]; ok {        // READ LOCK
            pool.fileMap[f.RelativeFilepath].Parent = parent            // WRITE LOCK
        }
        pool.fileMap[f.RelativeDirectory].Children = 
            append(pool.fileMap[f.RelativeDirectory].Children, f)       // WRITE LOCK
    }
}
```


### Достоинства
- отсутствие необходимости использования стороннего хранилища (СУБД/NoSQL) 
- возможность запуска приложения на основе существующих файлов

    Вышеперечисленное не играет роли из-за необходимости наличия пермиссий и их соотношений с файловой структурой, т.е. операций с БД не избежать.

### Недостатки
- усложнение кода и, как следствие, большая вероятность ошибок 
- возрастание сложности разработки и поддержки проекта
- усложнение кластерного развертывания из-за локальной природы источника метаданных
- увеличение расхода памяти с линейной зависимостью от кол-ва файлов/директорий, пользователей/групп, наборов разрешений
- использование "быстрого" индекса, полностью расположенного в памяти, вместе с "тяжелыми" операциями чтения и записи файлов не в состоянии обеспечить заметный прирост производительности работы проекта
- необходимость Read/Write блокировок `ctx.fileobjects` уничтожает потенциальный выигрыш или, возможно, ведет к деградации скорости работы проекта (см. комментарии листинга `InjectFileObjects`)
- перестроение индекса метаданных (`ctx.fileobjects`), расположенных на том же носителе, на котором осуществляются "тяжелые" файловые операции, ведет к деградации скорости проекта (см. комментарии листинга `RebuildStorageIndex`)
    
    Пояснение: с точки зрения контроллера носителя есть только 2 типа операций: чтение и запись, поэтому "высокоуровневые" операции чтения/записи части данных файла в/из буфера памяти (`read(file, buffer)`, `write(file, buffer)`), равно как и чтение метаданных (`stat(file)`), являются атомарными и равноценными, а "быстрая" операция чтения из индекса будет выполнена в свою очередь после "медленной" операции чтения или записи буфера данных в файл по принципу FIFO. 

    Linux buffer cache does not actually buffer files, but blocks, which are the smallest units of disk I/O (under Linux, they are usually 1 KB). This way, also directories, super blocks, other filesystem bookkeeping data, and non-filesystem disks are cached (http://www.tldp.org/LDP/sag/html/buffer-cache.html). 

## Доработка

### Возможность прерывания процесса чтения/записи 
- пример: https://github.com/northbright/ctx/blob/master/ctxcopy/ctxcopy.go

### Приоритизация процессов чтения/записи
* добавление приоритетов, пример:

```golang
type Priority int

const (
    HIGHEST Priority = iota
    HIGH
    NORMAL
    LOW
    LOWEST
)
```

* в глобальный контект добавить переменную для таймаута, который будет динамически изменяться в зависимости от текущей нагрузки

* в функции чтения\записи добавить операцию ожидания, пример:

```golang
func FuncName (ctx *AppContext, priority Priority, ...) {
    read/write(...)
    // since HIGHEST Priority equals 0, it means no delay for it
    time.Sleep(ctx.Timeout * priority * time.Millisecond)
}
```

### Lockless чтение файла
#### вариант 1 (требует проверки)
При необходимости изменения данных файла: 
* возврат ошибки в случае наличия "теневой" копии (`shadow`) оригинального файла (`original`)
* создание и запись данных в `shadow`
* переименование `shadow` в `original`

    Unix-like operating systems like Linux and Mac OS X don't lock the file but rather the underlying disk sectors. This may seem a trivial differentiation but it means that the file's record in the filesystem table of contents can be deleted without disturbing any program that already has the file open. So you can delete a file while it is still executing or otherwise in use and it will continue to exist on disk as long as some process has an open handle for it even though its entry in the file table is gone


#### вариант 2
##### чтение файла
```
if (shadow exists and not write_locked)
    file_to_read = shadow
else
    file_to_read = original

read_lock(file_to_read)
read(file_to_read)
read_unlock(file_to_read)
```

##### запись файла
```
if (shadow exists)
    error("file busy")

write_lock(shadow)
write(shadow)
write_unlock(shadow)

read_lock(shadow)
write_lock(original)
copy(shadow, original)
write_unlock(original)
read_unlock(shadow)

write_lock(shadow)
remove(shadow)
write_unlock(shadow)
```